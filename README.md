# MTB-NTM

#### Description
This repository is used for tracking the history of source code and push release which used for HongShi PCR experiment result automated interpretation.


#### Software Architecture
his plug-in is developed using C#, based on Visual Studio 2020 (version 17.4.3) Microsoft .NET Framework version 4.8.09032.


#### Installation

1.  Download correct dll plug-in and copy it to HongShi PCR software installation directory or anywhere you like;
2.  Open your PCR experiment file(such as Template.pcr), then click "Experiment Analysis" --->"Test Result"--->"Advanced Rules" chose
    the dll plug-in and click "Analysis" again;
3.  Once this dll plug-in was applied to the PCR experiment file on any computer, you need not try that again.

#### Instructions

1.  This dll plug-in based on Product Description strictly. Every change in Product Description may lead to dll plug-in upgrade;
2.  Before you use this dll plug-in, please confirm that you get the latest version;
3.  If you find any bug, contact me immediately or be sure to tell us about it in the comment below.

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request
