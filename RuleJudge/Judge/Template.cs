﻿using RuleJudge.RuleData;
using SLANCommon;
using SLANCommon.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;

namespace RuleJudge.Judge
{
    public class Template : BaseJudge
    {
        /// <summary>
        /// 构造函数，需要定义项目类型，管，通道信息
        /// </summary>
        public Template()
        {
            Type = EProjectType.AQ;
            TubesNo = new List<int>() { 1, 2 };
            ChannelsNo = new List<int>() { 1, 1 };
        }

        /// <summary>
        /// 判断逻辑
        /// 规则中只有结论（Conclusion）和自定义结果（CustomResults）可以回传到SLAN软件，其他数据都是为了编辑规则得出结论用的，不能回传。
        /// </summary>
        /// <param name="samples">样本列表</param>
        /// <returns>非空：SLAN会弹出内容信息框，进行提示</returns>
        public override string Judge(List<Sample> samples)
        {
            //用户可以根据SLAN语言出信息 0:中文 1：英文
            if (Language == 0)
            {
                //中文
            }
            else if (Language == 1)
            {
                //英文
            }

            //在结果判断之前，先检查是否有需要判断的样本
            if (samples.Count == 0) return "无判断样本！";

            //2. 阴性对照的判读
            List<Sample> NegativeList = samples.Where(s => s.Type == SampleType.Negative).ToList();
            foreach (Sample sample in NegativeList)
            {
                bool NegativeIC = true;//判断质控品
                sample.Conclusion.Add(new Conclusion() { Content = NegativeIC == true ? "合格" : "不合格" });
            }
            //3. 阳性对照的判读
            List<Sample> PositiveList = samples.Where(s => s.Type == SampleType.HighPositive).ToList();
            foreach (Sample sample in PositiveList)
            {
                bool PositiveIC = true;//判断质控品
                sample.Conclusion.Add(new Conclusion() { Content = PositiveIC == true ? "合格" : "不合格" });
            }

            List<Sample> UnknownList = samples.Where(s => s.Type == SampleType.Unknown).ToList();   //取出所有“待测样本”
            foreach (Sample sample in UnknownList)
            {
                //获取样本中第一个目标CT值
                double ct = sample.Targets[0].Result.Ct;
                //获取样本中第一个管中第一个目标CT值
                ct = sample.Tubes[0].Targets[0].Result.Ct;
                //可以通过Result获取目标结果，比如获取原始曲线
                List<Dot> list = sample.Tubes[0].Targets[0].Result.RawCurve;

                //判断是否为NoCt，NoCt就是double无穷大
                if (double.IsPositiveInfinity(ct) == true)
                {

                }

                //获取参数
                Paramter paramter = Paramters.Where(s => s.TargetName == sample.Targets[0].Name).FirstOrDefault();
                //根据y=ax+b进行计算，并增加大中间变量中
                double y = paramter.ParamterA * sample.Targets[0].Result.Ct + paramter.ParamterB;
                sample.Targets[0].CustomResults.Add(new CustomResult() { Name = "定量结果", Content = y.ToString("0.0000") });


                //目标结论：
                sample.Targets[0].Conclusion.Add(new Conclusion() { Content = "XX" });

                //管结论：
                sample.Tubes[0].Conclusion.Add(new Conclusion() { Content = "XX" });

                //样本结论
                sample.Conclusion.Add(new Conclusion() { Content = "XX" });

                //目标中间结果：
                sample.Targets[0].CustomResults.Add(new CustomResult() { Name = "XX", Content = "XX" });

                //管中间结果：
                sample.Tubes[0].CustomResults.Add(new CustomResult() { Name = "XX", Content = "XX" });

                //样本中间结果：
                sample.CustomResults.Add(new CustomResult() { Name = "XX", Content = "XX" });
            }

            return string.Empty;
        }
    }
}
