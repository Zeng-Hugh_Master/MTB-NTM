﻿// RuleJudge.Judge.MTB_NTM
using System.Collections.Generic;
using System.Linq;
using System.Windows.Media;
using System.Windows.Forms;
using RuleJudge.Judge;
using RuleJudge.RuleData;
using SLANCommon.Enum;

public class MTB_NTM : BaseJudge
{
    public MTB_NTM()
    {
        Type = EProjectType.AQ;
        base.TubesNo = new List<int> { 1, 1, 1 };
        base.ChannelsNo = new List<int> { 1, 2, 3 };
    }

    int FAM = 0;
    int VIC = 1;
    int ROX = 2;
    int CY5 = 3;

    bool NC_Check_Pass = false;
    bool PC_Check_Pass = false;
    bool Leak_NC_Control = false;
    bool Leak_PC_Control = false;

    const double IS6110_Cutoff = 35.57;  // 结核分枝杆菌 MTB
    const double NTM_Cutoff = 35.06;  // 非结核分枝杆菌 NTNM
    const double IC_Cutoff = 35.00;  // 内参（标）IC


    // 反应管各通道检测靶标
    //      0         1        2    
    //  |---------------------------
    //  |   FAM  |   VIC  |   ROX  |
    //  | IS6110 |  NTM   |   ICU  |
    //  |--------------------------|

    // 😹🐱‍🐉🐵🐶🐺🐱🦁🐯🦒🦊🦝🐮🐷🐗🐭🐹🐰🐻🐨🐼🐸🦓🐴🦄🐔🐲🐒🐕‍🦺🐫🦘🦙🐏🐁🐘🦨🦡🦛🦏🐪🐇🦔🐀🐿🦎🐊🐢🐍🐋🐬🐉🐟🐠🐡🦐🦑🐙🦞🦀🦜🦆🐓🦚🦉🐦🐤🦗🐜🐝🐌🦋从🕷🐞🦂🦇🦃🦅
    // 开始因阳性质控、待测(未知)样本判读
    public override string Judge(List<Sample> samples)
    {
        if (samples.Count == 0)
        {
            return "无判断样本！";
        }

        #region 阴性指控判断
        List<Sample> NC_list = samples.Where((Sample s) => s.Type == SampleType.Negative).ToList();
        if (NC_list.Count != 0)
        {
            foreach (Sample NC in NC_list)
            {
                NC_Check_Pass = (NC.Tubes[0].Targets[FAM].Result.Ct > IS6110_Cutoff && NC.Tubes[0].Targets[VIC].Result.Ct > NTM_Cutoff && NC.Tubes[0].Targets[ROX].Result.Ct <= IC_Cutoff);
                NC.Tubes[0].Targets[VIC].Conclusion.Add(new Conclusion
                {
                    Content = NC_Check_Pass ? "阴性质控合格" : "阴性质控不合格",
                    Color = NC_Check_Pass ? Colors.Green : Colors.Red,
                    BackgroundColor = NC_Check_Pass ? Colors.LightGreen : Colors.LightYellow,
                    ForegroundColor = NC_Check_Pass ? Colors.Red : Colors.Blue,
                });
            }
        }
        else
        {
            Leak_NC_Control = true;
        }
        #endregion

        // 😹🐱‍🐉🐵🐶🐺🐱🦁🐯🦒🦊🦝🐮🐷🐗🐭🐹🐰🐻🐨🐼🐸🦓🐴🦄🐔🐲🐒🐕‍🦺🐫🦘🦙🐏🐁🐘🦨🦡🦛🦏🐪🐇🦔🐀🐿🦎🐊🐢🐍🐋🐬🐉🐟🐠🐡🦐🦑🐙🦞🦀🦜🦆🐓🦚🦉🐦🐤🦗🐜🐝🐌🦋从🕷🐞🦂🦇🦃🦅
        #region 阳性质控判读
        List<Sample> PC_list = samples.Where((Sample s) => s.Type == SampleType.HighPositive).ToList();
        if (PC_list.Count != 0)
        {
            foreach (Sample PC in PC_list)
            {
                PC_Check_Pass = (PC.Tubes[0].Targets[FAM].Result.Ct <= IS6110_Cutoff && PC.Tubes[0].Targets[VIC].Result.Ct <= NTM_Cutoff && PC.Tubes[0].Targets[ROX].Result.Ct <= IC_Cutoff);
                PC.Tubes[0].Targets[VIC].Conclusion.Add(new Conclusion
                {
                    Content = PC_Check_Pass ? "阳性质控合格" : "阳性质控不合格",
                    Color = PC_Check_Pass ? Colors.Green : Colors.Red,
                    BackgroundColor = PC_Check_Pass ? Colors.LightGreen : Colors.LightYellow,
                    ForegroundColor = PC_Check_Pass ? Colors.Red : Colors.Blue,
                });
            }
        }
        else
        {
            Leak_PC_Control = true;
        }
        #endregion

        // 😹🐱‍🐉🐵🐶🐺🐱🦁🐯🦒🦊🦝🐮🐷🐗🐭🐹🐰🐻🐨🐼🐸🦓🐴🦄🐔🐲🐒🐕‍🦺🐫🦘🦙🐏🐁🐘🦨🦡🦛🦏🐪🐇🦔🐀🐿🦎🐊🐢🐍🐋🐬🐉🐟🐠🐡🦐🦑🐙🦞🦀🦜🦆🐓🦚🦉🐦🐤🦗🐜🐝🐌🦋从🕷🐞🦂🦇🦃🦅
        #region 待测样本判读
        List<Sample> UnknownSample_list = samples.Where((Sample s) => s.Type == SampleType.Unknown).ToList();
        foreach (Sample Unknown_Sample in UnknownSample_list)
        {
            // 样本内参是否合格:（1）反应孔有非内参靶标阳性；（2）反应孔靶标均为阴性，且内参阳性。
            bool IC_Check_Pass = false;
            if (Unknown_Sample.Tubes[0].Targets[FAM].Result.Ct <= IS6110_Cutoff ||
                Unknown_Sample.Tubes[0].Targets[VIC].Result.Ct <= NTM_Cutoff ||
                Unknown_Sample.Tubes[0].Targets[ROX].Result.Ct <= IC_Cutoff)
            {
                IC_Check_Pass = true;
            }
            if (!IC_Check_Pass)
            {
                Unknown_Sample.Tubes[0].Targets[VIC].Conclusion.Add(new Conclusion
                {
                    Content = "内参检测不合格，需重新检测！",
                    Color = Colors.Blue,
                    BackgroundColor = Colors.LightYellow,
                    ForegroundColor = Colors.LightCoral,
                });
                continue;
            }
            // 反应管A孔FAM通道：结核分枝杆菌（IS6110）
            Unknown_Sample.Tubes[0].Targets[FAM].Conclusion.Add(new Conclusion
            {
                Content = Unknown_Sample.Tubes[0].Targets[FAM].Result.Ct <= IS6110_Cutoff ? "结核分枝杆菌阳性" : "结核分枝杆菌阳性",
                Color = Unknown_Sample.Tubes[0].Targets[FAM].Result.Ct <= IS6110_Cutoff ? Colors.Red : Colors.Green,
                BackgroundColor = Unknown_Sample.Tubes[0].Targets[FAM].Result.Ct <= IS6110_Cutoff ? Colors.LightYellow : Colors.LightYellow,
            });
            // 反应管A孔VIC通道：非结核分枝杆菌（NTM）
            Unknown_Sample.Tubes[0].Targets[VIC].Conclusion.Add(new Conclusion
            {
                Content = Unknown_Sample.Tubes[0].Targets[VIC].Result.Ct <= NTM_Cutoff ? "非结核分枝杆菌阳性" : "非结核分枝杆菌阳性",
                Color = Unknown_Sample.Tubes[0].Targets[VIC].Result.Ct <= NTM_Cutoff ? Colors.Red : Colors.Green,
                BackgroundColor = Unknown_Sample.Tubes[0].Targets[VIC].Result.Ct <= NTM_Cutoff ? Colors.LightYellow : Colors.LightYellow,
            });
        }
        #endregion

        // 😹🐱‍🐉🐵🐶🐺🐱🦁🐯🦒🦊🦝🐮🐷🐗🐭🐹🐰🐻🐨🐼🐸🦓🐴🦄🐔🐲🐒🐕‍🦺🐫🦘🦙🐏🐁🐘🦨🦡🦛🦏🐪🐇🦔🐀🐿🦎🐊🐢🐍🐋🐬🐉🐟🐠🐡🦐🦑🐙🦞🦀🦜🦆🐓🦚🦉🐦🐤🦗🐜🐝🐌🦋从🕷🐞🦂🦇🦃🦅
        #region 重测样本判读
        List<Sample> RetestSample_list = samples.Where((Sample s) => s.Type == SampleType.RetestSample).ToList();
        foreach (Sample Retest_Sample in RetestSample_list)
        {
            // 样本内参是否合格:（1）反应孔有非内参靶标阳性；（2）反应孔靶标均为阴性，且内参阳性。
            bool IC_Check_Pass = false;
            if (Retest_Sample.Tubes[0].Targets[FAM].Result.Ct <= IS6110_Cutoff ||
                Retest_Sample.Tubes[0].Targets[VIC].Result.Ct <= NTM_Cutoff ||
                Retest_Sample.Tubes[0].Targets[ROX].Result.Ct <= IC_Cutoff)
            {
                IC_Check_Pass = true;
            }
            if (!IC_Check_Pass)
            {
                Retest_Sample.Tubes[0].Targets[VIC].Conclusion.Add(new Conclusion
                {
                    Content = "内参检测不合格，需重新检测！",
                    Color = Colors.Blue,
                    BackgroundColor = Colors.LightYellow,
                    ForegroundColor = Colors.LightCoral,
                });
                continue;
            }
            // 反应管A孔FAM通道：结核分枝杆菌（IS6110）
            Retest_Sample.Tubes[0].Targets[FAM].Conclusion.Add(new Conclusion
            {
                Content = Retest_Sample.Tubes[0].Targets[FAM].Result.Ct <= IS6110_Cutoff ? "结核分枝杆菌阳性" : "结核分枝杆菌阳性",
                Color = Retest_Sample.Tubes[0].Targets[FAM].Result.Ct <= IS6110_Cutoff ? Colors.Red : Colors.Green,
                BackgroundColor = Retest_Sample.Tubes[0].Targets[FAM].Result.Ct <= IS6110_Cutoff ? Colors.LightYellow : Colors.LightYellow,
            });
            // 反应管A孔VIC通道：非结核分枝杆菌（NTM）
            Retest_Sample.Tubes[0].Targets[VIC].Conclusion.Add(new Conclusion
            {
                Content = Retest_Sample.Tubes[0].Targets[VIC].Result.Ct <= NTM_Cutoff ? "非结核分枝杆菌阳性" : "非结核分枝杆菌阳性",
                Color = Retest_Sample.Tubes[0].Targets[VIC].Result.Ct <= NTM_Cutoff ? Colors.Red : Colors.Green,
                BackgroundColor = Retest_Sample.Tubes[0].Targets[VIC].Result.Ct <= NTM_Cutoff ? Colors.LightYellow : Colors.LightYellow,
            });
        }
        #endregion

        /* *****************************************************************************************************
        * 实验中未设置阴阳性质控不影响判读，仅给出弹窗提醒缺少质控 *************************************************
        * 阴阳性质控不合格的，则给出提醒，但仍然进行判读 **********************************************************
        * *****************************************************************************************************
        */
        #region 质控结果异常（不合格，缺失）提示
        if (!NC_Check_Pass || !PC_Check_Pass)
        {
            System.Windows.Forms.MessageBox.Show("一个或多个阴阳性质控不符合要求！是否忽略？", "质控提示", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }
        if (Leak_NC_Control || Leak_PC_Control)
        {
            System.Windows.Forms.MessageBox.Show("实验缺少阴性质控或阳性质控！是否忽略？", "质控提示", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }
        #endregion

        return string.Empty;
    }
}