﻿using RuleJudge.RuleData;
using SLANCommon;
using SLANCommon.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;

namespace RuleJudge.Judge
{
    public class HR2 : BaseJudge
    {
        /// <summary>
        /// 构造函数，需要定义项目类型，管，通道信息
        /// </summary>
        public HR2()
        {
            Type = EProjectType.Melt;
            TubesNo = new List<int>() { 1, 1, 1, 1, 2, 2, 2, 2 };
            ChannelsNo = new List<int>() { 1, 2, 3, 4, 1, 2, 3, 4 };
        }
        /// <summary>
        /// 判断逻辑
        /// 规则中只有结论（Conclusion）和自定义结果（CustomResults）可以回传到SLAN软件，其他数据都是为了编辑规则得出结论用的，不能回传。
        /// </summary>
        /// <param name="samples">样本列表</param>
        /// <returns>非空：SLAN会弹出内容信息框，进行提示</returns>
        public override string Judge(List<Sample> samples)
        {
            //在结果判断之前，先检查是否有需要判断的样本
            if (samples.Count == 0) return "无判断样本！";

            //2. 阴性对照的判读
            //List<Sample> NegativeList = samples.Where(s => s.Type == SampleType.Negative).ToList();
            //foreach (Sample sample in NegativeList)
            //{
            //    bool NegativeIC = true;//判断质控品
            //    sample.Conclusion.Add(new Conclusion() { Content = NegativeIC == true ? "合格" : "不合格" });
            //}
            //3. 阳性对照的判读
            //List<Sample> PositiveList = samples.Where(s => s.Type == SampleType.HighPositive).ToList();
            //foreach (Sample sample in PositiveList)
            //{
            //    bool PositiveIC = true;//判断质控品
            //    sample.Conclusion.Add(new Conclusion() { Content = PositiveIC == true ? "合格" : "不合格" });
            //}

            List<Sample> UnknownList = samples.Where(s => s.Type == SampleType.Unknown).ToList();   //取出所有“待测样本”
            foreach (Sample sample in UnknownList)
            {
                #region A孔FAM
                Target target = sample.Tubes[0].Targets[0];
                MeltingPeak peek49 = target.Peaks.Where(s => Math.Abs(s.Tm - 49) <= 1.5).FirstOrDefault();
                MeltingPeak peek62 = target.Peaks.Where(s => Math.Abs(s.Tm - 62) <= 1.5).FirstOrDefault();
                MeltingPeak peek70 = target.Peaks.Where(s => Math.Abs(s.Tm - 70) <= 1.5).FirstOrDefault();
                if (peek49 != null &&
                        ((peek62 != null && peek70 != null && peek49.YValue > 20 && peek49.Rm > 8)
                        || (peek62 != null && peek70 == null && peek49.YValue > 17 && peek49.Rm > 8)
                        || (peek62 == null && peek70 != null && peek49.YValue > 17 && peek49.Rm > 8)
                        || (peek62 == null && peek70 == null && peek49.YValue > 17 && peek49.Rm > 8)
                        ))
                {
                    target.Conclusion.Add(new Conclusion() { Content = "甲型流感病毒 阳性" });
                }
                else
                {
                    target.Conclusion.Add(new Conclusion() { Content = "甲型流感病毒 阴性" });
                }
                
                if (peek62 != null &&
                    ((peek49 != null && peek70 != null && peek62.YValue > 21 && peek62.Rm > 10)
                    || (peek49 != null && peek70 == null && peek62.YValue > 21 && peek62.Rm > 10)
                    || (peek49 == null && peek70 != null && peek62.YValue > 21 && peek62.Rm > 10)
                    || (peek49 == null && peek70 == null && peek62.YValue > 21 && peek62.Rm > 10)
                    ))
                {
                    target.Conclusion.Add(new Conclusion() { Content = "肺炎链球菌 阳性" });
                }
                else
                {
                    target.Conclusion.Add(new Conclusion() { Content = "肺炎链球菌 阴性" });
                }

                if (peek70 != null &&
                    ((peek49 != null && peek62 != null && peek70.YValue > 20 && peek70.Rm > 8)
                    || (peek49 != null && peek62 == null && peek70.YValue > 17 && peek70.Rm > 8)
                    || (peek49 == null && peek62 != null && peek70.YValue > 17 && peek70.Rm > 8)
                    || (peek49 == null && peek62 == null && peek70.YValue > 17 && peek70.Rm > 8)
                    ))
                {
                    target.Conclusion.Add(new Conclusion() { Content = "乙型流感病毒 阳性" });
                }
                else
                {
                    target.Conclusion.Add(new Conclusion() { Content = "乙型流感病毒 阴性" });
                }
                #endregion

                #region A孔VIC
                target = sample.Tubes[0].Targets[1];
                MeltingPeak peek63 = target.Peaks.Where(s => Math.Abs(s.Tm - 63) <= 1.5).FirstOrDefault();
                MeltingPeak peek71 = target.Peaks.Where(s => Math.Abs(s.Tm - 71) <= 1.5).FirstOrDefault();
                if (peek63 != null && peek63.YValue > 20 && peek63.Rm > 10)
                {
                    target.Conclusion.Add(new Conclusion() { Content = "副流感病毒3 阳性" });
                }
                else
                {
                    target.Conclusion.Add(new Conclusion() { Content = "副流感病毒3 阴性" });
                }
                if (peek71 != null && peek71.YValue > 20 && peek71.Rm > 10)
                {
                    target.Conclusion.Add(new Conclusion() { Content = "冠状病毒 229E 阳性" });
                }
                else
                {
                    target.Conclusion.Add(new Conclusion() { Content = "冠状病毒 229E 阴性" });
                }
                #endregion

                #region A孔ROX
                target = sample.Tubes[0].Targets[2];
                MeltingPeak peek55 = target.Peaks.Where(s => Math.Abs(s.Tm - 55) <= 1.5).FirstOrDefault();
                peek63 = target.Peaks.Where(s => Math.Abs(s.Tm - 63) <= 1.5).FirstOrDefault();
                MeltingPeak peek72 = target.Peaks.Where(s => Math.Abs(s.Tm - 72) <= 1.5).FirstOrDefault();
                if (peek55 != null &&
                    ((peek63 != null && peek72 != null && peek55.YValue > 24 && peek55.Rm > 10)
                    || (peek63 != null && peek72 == null && peek55.YValue > 24 && peek55.Rm > 10)
                    || (peek63 == null && peek72 != null && peek55.YValue > 28 && peek55.Rm > 10)
                    || (peek63 == null && peek72 == null && peek55.YValue > 28 && peek55.Rm > 10)
                    ))
                {
                    target.Conclusion.Add(new Conclusion() { Content = "副流感病毒4 阳性" });
                }
                else
                {
                    target.Conclusion.Add(new Conclusion() { Content = "副流感病毒4 阴性" });
                }

                if (peek63 != null &&
                    ((peek55 != null && peek72 != null && peek63.YValue > 24 && peek63.Rm > 8)
                    || (peek55 != null && peek72 == null && peek63.YValue > 24 && peek63.Rm > 10)
                    || (peek55 == null && peek72 != null && peek63.YValue > 32 && peek63.Rm > 8)
                    || (peek55 == null && peek72 == null && peek63.YValue > 32 && peek63.Rm > 10)
                    ))
                {
                    target.Conclusion.Add(new Conclusion() { Content = "肺炎衣原体 阳性" });
                }
                else
                {
                    target.Conclusion.Add(new Conclusion() { Content = "肺炎衣原体 阴性" });
                }

                if (peek72 != null &&
                    ((peek55 != null && peek63 != null && peek72.YValue > 30 && peek72.Rm > 8)
                    || (peek55 != null && peek63 == null && peek72.YValue > 27 && peek72.Rm > 10)
                    || (peek55 == null && peek63 != null && peek72.YValue > 27 && peek72.Rm > 8)
                    || (peek55 == null && peek63 == null && peek72.YValue > 27 && peek72.Rm > 10)
                    ))
                {
                    target.Conclusion.Add(new Conclusion() { Content = "百日咳杆菌 阳性" });
                }
                else
                {
                    target.Conclusion.Add(new Conclusion() { Content = "百日咳杆菌 阴性" });
                }
                #endregion

                #region A孔CY5
                target = sample.Tubes[0].Targets[3];
                MeltingPeak peek51 = target.Peaks.Where(s => Math.Abs(s.Tm - 51) <= 1.5).FirstOrDefault();
                MeltingPeak peek60 = target.Peaks.Where(s => Math.Abs(s.Tm - 60) <= 1.5).FirstOrDefault();
                peek71 = target.Peaks.Where(s => Math.Abs(s.Tm - 71) <= 1.5).FirstOrDefault();
                if (peek51 != null &&
                    ((peek60 != null && peek71 != null && peek51.YValue > 32 && peek51.Rm > 10)
                    || (peek60 != null && peek71 == null && peek51.YValue > 26 && peek51.Rm > 10)
                    || (peek60 == null && peek71 != null && peek51.YValue > 26 && peek51.Rm > 10)
                    || (peek60 == null && peek71 == null && peek51.YValue > 26 && peek51.Rm > 10)
                    ))
                {
                    target.Conclusion.Add(new Conclusion() { Content = "偏肺病毒 阳性" });
                }
                else
                {
                    target.Conclusion.Add(new Conclusion() { Content = "偏肺病毒 阴性" });
                }

                if (peek60 != null &&
                    ((peek51 != null && peek71 != null && peek60.YValue > 24 && peek60.Rm > 10)
                    || (peek51 != null && peek71 == null && peek60.YValue > 18 && peek60.Rm > 10)
                    || (peek51 == null && peek71 != null && peek60.YValue > 18 && peek60.Rm > 8)
                    || (peek51 == null && peek71 == null && peek60.YValue > 15 && peek60.Rm > 8)
                    ))
                {
                    target.Conclusion.Add(new Conclusion() { Content = "肺炎支原体 阳性" });
                }
                else
                {
                    target.Conclusion.Add(new Conclusion() { Content = "肺炎支原体 阴性" });
                }

                if (peek71 != null &&
                    ((peek51 != null && peek60 != null && peek71.YValue > 14 && peek71.Rm > 6)
                    || (peek51 != null && peek60 == null && peek71.YValue > 14 && peek71.Rm > 6)
                    || (peek51 == null && peek60 != null && peek71.YValue > 14 && peek71.Rm > 6)
                    || (peek51 == null && peek60 == null && peek71.YValue > 14 && peek71.Rm > 6)
                    ))
                {
                    target.Conclusion.Add(new Conclusion() { Content = "流感嗜血杆菌 阳性" });
                }
                else
                {
                    target.Conclusion.Add(new Conclusion() { Content = "流感嗜血杆菌 阴性" });
                }
                #endregion

                #region B孔FAM
                target = sample.Tubes[1].Targets[0];
                MeltingPeak peek52 = target.Peaks.Where(s => Math.Abs(s.Tm - 52) <= 1.5).FirstOrDefault();
                MeltingPeak peek61 = target.Peaks.Where(s => Math.Abs(s.Tm - 61) <= 1.5).FirstOrDefault();
                peek70 = target.Peaks.Where(s => Math.Abs(s.Tm - 70) <= 1.5).FirstOrDefault();
                if (peek52 != null &&
                    ((peek61 != null && peek70 != null && peek52.YValue > 25 && peek52.Rm > 10)
                    || (peek61 != null && peek70 == null && peek52.YValue > 20 && peek52.Rm > 10)
                    || (peek61 == null && peek70 != null && peek52.YValue > 20 && peek52.Rm > 10)
                    || (peek61 == null && peek70 == null && peek52.YValue > 20 && peek52.Rm > 10)
                    ))
                {
                    target.Conclusion.Add(new Conclusion() { Content = "呼吸道合胞病毒B 阳性" });
                }
                else
                {
                    target.Conclusion.Add(new Conclusion() { Content = "呼吸道合胞病毒B 阴性" });
                }

                if (peek61 != null &&
                    ((peek52 != null && peek70 != null && peek61.YValue > 25 && peek61.Rm > 10)
                    || (peek52 != null && peek70 == null && peek61.YValue > 19 && peek61.Rm > 10)
                    || (peek52 == null && peek70 != null && peek61.YValue > 19 && peek61.Rm > 10)
                    || (peek52 == null && peek70 == null && peek61.YValue > 19 && peek61.Rm > 10)
                    ))
                {
                    target.Conclusion.Add(new Conclusion() { Content = "冠状病毒OC43 阳性" });
                }
                else
                {
                    target.Conclusion.Add(new Conclusion() { Content = "冠状病毒OC43 阴性" });
                }

                if (peek70 != null &&
                    ((peek52 != null && peek61 != null && peek70.YValue > 20 && peek70.Rm > 10)
                    || (peek52 != null && peek61 == null && peek70.YValue > 20 && peek70.Rm > 10)
                    || (peek52 == null && peek61 != null && peek70.YValue > 20 && peek70.Rm > 10)
                    || (peek52 == null && peek61 == null && peek70.YValue > 20 && peek70.Rm > 10)
                    ))
                {
                    target.Conclusion.Add(new Conclusion() { Content = "嗜肺军团菌 阳性" });
                }
                else
                {
                    target.Conclusion.Add(new Conclusion() { Content = "嗜肺军团菌 阴性" });
                }
                #endregion

                #region B孔VIC
                target = sample.Tubes[1].Targets[1];
                peek55 = target.Peaks.Where(s => Math.Abs(s.Tm - 55) <= 1.5).FirstOrDefault();
                MeltingPeak peek64 = target.Peaks.Where(s => Math.Abs(s.Tm - 64) <= 1.5).FirstOrDefault();
                peek70 = target.Peaks.Where(s => Math.Abs(s.Tm - 70) <= 1.5).FirstOrDefault();
                if (peek55 != null &&
                    ((peek64 != null && peek70 != null && peek55.YValue > 25 && peek55.Rm > 8)
                    || (peek64 != null && peek70 == null && peek55.YValue > 20 && peek55.Rm > 8)
                    || (peek64 == null && peek70 != null && peek55.YValue > 20 && peek55.Rm > 10)
                    || (peek64 == null && peek70 == null && peek55.YValue > 20 && peek55.Rm > 10)
                    ))
                {
                    target.Conclusion.Add(new Conclusion() { Content = "鼻病毒 阳性" });
                }
                else
                {
                    target.Conclusion.Add(new Conclusion() { Content = "鼻病毒 阴性" });
                }

                if (peek64 != null &&
                    ((peek55 != null && peek70 != null && peek64.YValue > 20 && peek64.Rm > 7)
                    || (peek55 != null && peek70 == null && peek64.YValue > 20 && peek64.Rm > 7)
                    || (peek55 == null && peek70 != null && peek64.YValue > 20 && peek64.Rm > 7)
                    || (peek55 == null && peek70 == null && peek64.YValue > 20 && peek64.Rm > 10)
                    ))
                {
                    target.Conclusion.Add(new Conclusion() { Content = "冠状病毒NL63 阳性" });
                }
                else
                {
                    target.Conclusion.Add(new Conclusion() { Content = "冠状病毒NL63 阴性" });
                }

                if (peek70 != null &&
                   ((peek55 != null && peek64 != null && peek70.YValue > 18 && peek70.Rm > 10)
                   || (peek55 != null && peek64 == null && peek70.YValue > 18 && peek70.Rm > 10)
                   || (peek55 == null && peek64 != null && peek70.YValue > 18 && peek70.Rm > 10)
                   || (peek55 == null && peek64 == null && peek70.YValue > 18 && peek70.Rm > 10)
                   ))
                {
                    target.Conclusion.Add(new Conclusion() { Content = "内参 阳性" });
                }
                else
                {
                    target.Conclusion.Add(new Conclusion() { Content = "内参 阴性" });
                }
                #endregion

                #region B孔ROX
                target = sample.Tubes[1].Targets[2];
                peek52 = target.Peaks.Where(s => Math.Abs(s.Tm - 52) <= 1.5).FirstOrDefault();
                peek63 = target.Peaks.Where(s => Math.Abs(s.Tm - 63) <= 1.5).FirstOrDefault();
                if (peek52 != null && peek52.YValue > 20 && peek52.Rm > 8)
                {
                    target.Conclusion.Add(new Conclusion() { Content = "呼吸道合胞病毒A 阳性" });
                }
                else
                {
                    target.Conclusion.Add(new Conclusion() { Content = "呼吸道合胞病毒A 阴性" });
                }

                if (peek63 != null && peek63.YValue > 24 && peek63.Rm > 10)
                {
                    target.Conclusion.Add(new Conclusion() { Content = "冠状病毒HKU1 阳性" });
                }
                else
                {
                    target.Conclusion.Add(new Conclusion() { Content = "冠状病毒HKU1 阴性" });
                }
                #endregion

                #region B孔CY5
                target = sample.Tubes[1].Targets[3];
                peek51 = target.Peaks.Where(s => Math.Abs(s.Tm - 51) <= 1.5).FirstOrDefault();
                peek61 = target.Peaks.Where(s => Math.Abs(s.Tm - 61) <= 1.5).FirstOrDefault();
                MeltingPeak peek67 = target.Peaks.Where(s => Math.Abs(s.Tm - 67) <= 1.5).FirstOrDefault();
                if (peek51 != null &&
                    ((peek61 != null && peek67 != null && peek51.YValue > 25 && peek51.Rm > 8)
                    || (peek61 != null && peek67 == null && peek51.YValue > 20 && peek51.Rm > 8)
                    || (peek61 == null && peek67 != null && peek51.YValue > 15 && peek51.Rm > 8)
                    || (peek61 == null && peek67 == null && peek51.YValue > 15 && peek51.Rm > 8)
                    ))
                {
                    target.Conclusion.Add(new Conclusion() { Content = "腺病毒 阳性" });
                }
                else
                {
                    target.Conclusion.Add(new Conclusion() { Content = "腺病毒 阴性" });
                }

                if (peek61 != null &&
                    ((peek51 != null && peek67 != null && peek61.YValue > 24 && peek61.Rm > 8)
                    || (peek51 != null && peek67 == null && peek61.YValue > 20 && peek61.Rm > 8)
                    || (peek51 == null && peek67 != null && peek61.YValue > 24 && peek61.Rm > 8)
                    || (peek51 == null && peek67 == null && peek61.YValue > 17 && peek61.Rm > 8)
                    ))
                {
                    target.Conclusion.Add(new Conclusion() { Content = "副流感病毒1 阳性" });
                }
                else
                {
                    target.Conclusion.Add(new Conclusion() { Content = "副流感病毒1 阴性" });
                }

                if (peek67 != null &&
                    ((peek51 != null && peek61 != null && peek67.YValue > 13 && peek67.Rm > 6)
                    || (peek51 != null && peek61 == null && peek67.YValue > 13 && peek67.Rm > 6)
                    || (peek51 == null && peek61 != null && peek67.YValue > 13 && peek67.Rm > 6)
                    || (peek51 == null && peek61 == null && peek67.YValue > 13 && peek67.Rm > 6)
                    ))
                {
                    target.Conclusion.Add(new Conclusion() { Content = "副流感病毒2 阳性" });
                }
                else
                {
                    target.Conclusion.Add(new Conclusion() { Content = "副流感病毒2 阴性" });
                }
                #endregion

                #region 样本结论
                if (sample.Tubes[1].Targets.Where(s => s.Conclusion.Where(s1 => s1.Content.Contains("阳性")).Count() > 0).Count() == 0)
                {
                    sample.Conclusion.Add(new Conclusion() { Content = "内参异常，建议重新检测" });
                    continue;
                }
                if (sample.Tubes[1].Targets[0].Conclusion.Where(s => s.Content == "呼吸道合胞病毒B 阳性").Count() == 0
                    && sample.Tubes[1].Targets[2].Conclusion.Where(s => s.Content == "呼吸道合胞病毒A 阳性").Count() == 0)
                {
                    //sample.Conclusion.Add(new Conclusion() { Content = "呼吸道合胞病毒 未检出" });
                    //sample.CustomResults.Add(new CustomResult() { Name = "呼吸道合胞病毒", Content = "未检出" });
                }
                else
                {
                    sample.Conclusion.Add(new Conclusion() { Content = "呼吸道合胞病毒 检出" });
                    //sample.CustomResults.Add(new CustomResult() { Name = "呼吸道合胞病毒", Content = "检出" });
                }
                if (sample.Tubes[0].Targets[1].Conclusion.Where(s => s.Content == "冠状病毒 229E 阳性").Count() == 0
                    && sample.Tubes[1].Targets[0].Conclusion.Where(s => s.Content == "冠状病毒OC43 阳性").Count() == 0)
                {
                    //sample.Conclusion.Add(new Conclusion() { Content = "冠状病毒 未检出" });
                    //sample.CustomResults.Add(new CustomResult() { Name = "冠状病毒", Content = "未检出" });
                }
                else
                {
                    sample.Conclusion.Add(new Conclusion() { Content = "冠状病毒 检出" });
                    //sample.CustomResults.Add(new CustomResult() { Name = "冠状病毒", Content = "检出" });
                }
                if (sample.Tubes[0].Targets[1].Conclusion.Where(s => s.Content == "副流感病毒3 阳性").Count() == 0
                    && sample.Tubes[0].Targets[2].Conclusion.Where(s => s.Content == "副流感病毒4 阳性").Count() == 0
                    && sample.Tubes[1].Targets[3].Conclusion.Where(s => s.Content == "副流感病毒1 阳性").Count() == 0
                    && sample.Tubes[1].Targets[3].Conclusion.Where(s => s.Content == "副流感病毒2 阳性").Count() == 0)
                {
                    //sample.Conclusion.Add(new Conclusion() { Content = "副流感病毒 未检出" });
                    //sample.CustomResults.Add(new CustomResult() { Name = "副流感病毒", Content = "未检出" });
                }
                else
                {
                    sample.Conclusion.Add(new Conclusion() { Content = "副流感病毒 检出" });
                    //sample.CustomResults.Add(new CustomResult() { Name = "副流感病毒", Content = "检出" });
                }
                if (sample.Tubes[0].Targets[0].Conclusion.Where(s => s.Content == "甲型流感病毒 阳性").Count() == 0)
                {
                    //sample.Conclusion.Add(new Conclusion() { Content = "甲型流感病毒 未检出" });
                    //sample.CustomResults.Add(new CustomResult() { Name = "甲型流感病毒", Content = "未检出" });
                }
                else
                {
                    sample.Conclusion.Add(new Conclusion() { Content = "甲型流感病毒 检出" });
                    //sample.CustomResults.Add(new CustomResult() { Name = "甲型流感病毒", Content = "检出" });
                }
                if (sample.Tubes[0].Targets[0].Conclusion.Where(s => s.Content == "肺炎链球菌 阳性").Count() == 0)
                {
                    //sample.Conclusion.Add(new Conclusion() { Content = "肺炎链球菌 未检出" });
                    //sample.CustomResults.Add(new CustomResult() { Name = "肺炎链球菌", Content = "未检出" });
                }
                else
                {
                    sample.Conclusion.Add(new Conclusion() { Content = "肺炎链球菌 检出" });
                    //sample.CustomResults.Add(new CustomResult() { Name = "肺炎链球菌", Content = "检出" });
                }
                if (sample.Tubes[0].Targets[0].Conclusion.Where(s => s.Content == "乙型流感病毒 阳性").Count() == 0)
                {
                    //sample.Conclusion.Add(new Conclusion() { Content = "乙型流感病毒 未检出" });
                    //sample.CustomResults.Add(new CustomResult() { Name = "乙型流感病毒", Content = "未检出" });
                }
                else
                {
                    sample.Conclusion.Add(new Conclusion() { Content = "乙型流感病毒 检出" });
                    //sample.CustomResults.Add(new CustomResult() { Name = "乙型流感病毒", Content = "检出" });
                }
                if (sample.Tubes[0].Targets[2].Conclusion.Where(s => s.Content == "肺炎衣原体 阳性").Count() == 0)
                {
                    //sample.Conclusion.Add(new Conclusion() { Content = "肺炎衣原体 未检出" });
                    //sample.CustomResults.Add(new CustomResult() { Name = "肺炎衣原体", Content = "未检出" });
                }
                else
                {
                    sample.Conclusion.Add(new Conclusion() { Content = "肺炎衣原体 检出" });
                    //sample.CustomResults.Add(new CustomResult() { Name = "肺炎衣原体", Content = "检出" });
                }
                if (sample.Tubes[0].Targets[2].Conclusion.Where(s => s.Content == "百日咳杆菌 阳性").Count() == 0)
                {
                    //sample.Conclusion.Add(new Conclusion() { Content = "百日咳杆菌 未检出" });
                    //sample.CustomResults.Add(new CustomResult() { Name = "百日咳杆菌", Content = "未检出" });
                }
                else
                {
                    sample.Conclusion.Add(new Conclusion() { Content = "百日咳杆菌 检出" });
                    //sample.CustomResults.Add(new CustomResult() { Name = "百日咳杆菌", Content = "检出" });
                }
                if (sample.Tubes[0].Targets[3].Conclusion.Where(s => s.Content == "偏肺病毒 阳性").Count() == 0)
                {
                    //sample.Conclusion.Add(new Conclusion() { Content = "偏肺病毒 未检出" });
                    //sample.CustomResults.Add(new CustomResult() { Name = "偏肺病毒", Content = "未检出" });
                }
                else
                {
                    sample.Conclusion.Add(new Conclusion() { Content = "偏肺病毒 检出" });
                    //sample.CustomResults.Add(new CustomResult() { Name = "偏肺病毒", Content = "检出" });
                }
                if (sample.Tubes[0].Targets[3].Conclusion.Where(s => s.Content == "肺炎支原体 阳性").Count() == 0)
                {
                    //sample.Conclusion.Add(new Conclusion() { Content = "肺炎支原体 未检出" });
                    //sample.CustomResults.Add(new CustomResult() { Name = "肺炎支原体", Content = "未检出" });
                }
                else
                {
                    sample.Conclusion.Add(new Conclusion() { Content = "肺炎支原体 检出" });
                    //sample.CustomResults.Add(new CustomResult() { Name = "肺炎支原体", Content = "检出" });
                }
                if (sample.Tubes[0].Targets[3].Conclusion.Where(s => s.Content == "流感嗜血杆菌 阳性").Count() == 0)
                {
                    //sample.Conclusion.Add(new Conclusion() { Content = "流感嗜血杆菌 未检出" });
                    //sample.CustomResults.Add(new CustomResult() { Name = "流感嗜血杆菌", Content = "未检出" });
                }
                else
                {
                    sample.Conclusion.Add(new Conclusion() { Content = "流感嗜血杆菌 检出" });
                    //sample.CustomResults.Add(new CustomResult() { Name = "流感嗜血杆菌", Content = "检出" });
                }
                if (sample.Tubes[1].Targets[0].Conclusion.Where(s => s.Content == "嗜肺军团菌 阳性").Count() == 0)
                {
                    //sample.Conclusion.Add(new Conclusion() { Content = "嗜肺军团菌 未检出" });
                    //sample.CustomResults.Add(new CustomResult() { Name = "嗜肺军团菌", Content = "未检出" });
                }
                else
                {
                    sample.Conclusion.Add(new Conclusion() { Content = "嗜肺军团菌 检出" });
                    //sample.CustomResults.Add(new CustomResult() { Name = "嗜肺军团菌", Content = "检出" });
                }
                if (sample.Tubes[1].Targets[1].Conclusion.Where(s => s.Content == "鼻病毒 阳性").Count() == 0)
                {
                    //sample.Conclusion.Add(new Conclusion() { Content = "鼻病毒 未检出" });
                    //sample.CustomResults.Add(new CustomResult() { Name = "鼻病毒", Content = "未检出" });
                }
                else
                {
                    sample.Conclusion.Add(new Conclusion() { Content = "鼻病毒 检出" });
                    //sample.CustomResults.Add(new CustomResult() { Name = "鼻病毒", Content = "检出" });
                }
                if (sample.Tubes[1].Targets[3].Conclusion.Where(s => s.Content == "腺病毒 阳性").Count() == 0)
                {
                    //sample.Conclusion.Add(new Conclusion() { Content = "腺病毒 未检出" });
                    //sample.CustomResults.Add(new CustomResult() { Name = "腺病毒", Content = "未检出" });
                }
                else
                {
                    sample.Conclusion.Add(new Conclusion() { Content = "腺病毒 检出" });
                    //sample.CustomResults.Add(new CustomResult() { Name = "腺病毒", Content = "检出" });
                }
                #endregion
            }

            return string.Empty;
        }
    }
}
