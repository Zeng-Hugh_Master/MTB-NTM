﻿using RuleJudge.RuleData;
using SLANCommon.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RuleJudge.Judge
{
    public class BaseJudge
    {
        public int Language = AccessIniFile.ReadIntValue(AppDomain.CurrentDomain.BaseDirectory + "SLAN.ini", "Common", "Language");
        public EProjectType Type = EProjectType.AQ;          //判断规则所在项目的实验类型
        public List<int> TubesNo;
        public List<int> ChannelsNo;
        public List<string> Targets;
        public List<Paramter> Paramters;

        public virtual string Judge(List<Sample> samples)
        {
            return string.Empty;
        }
    }
}
