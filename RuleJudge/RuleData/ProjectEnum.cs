﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SLANCommon.Enum
{
    /// <summary>
    /// 项目类型
    /// </summary>
    [Serializable]
    public enum EProjectType
    {
        All = -1,
        /// <summary>
        /// 定性/定量
        /// </summary>
        AQ,
        /// <summary>
        /// 标准熔解曲线
        /// </summary>
        Melt,
        /// <summary>
        /// 等位基因鉴定
        /// </summary>
        AD,
        /// <summary>
        /// HRM
        /// </summary>
        HRM,
        /// <summary>
        /// 相对定量
        /// </summary>
        RQ,
        /// <summary>
        /// 等温扩增 Isothermal amplification
        /// </summary>
        IA,
        /// <summary>
        /// 反式定性定量 Qualitative and quantitative trans
        /// </summary>
        TQ,
        /// <summary>
        /// 实验运行过程中，实时计算结果
        /// </summary>
        FastCal,
        Last
    }


    /// <summary>优化方式
    /// 
    /// </summary>
    [Serializable]
    public enum EOptionzationMode
    {
        /// <summary>
        /// 不优化
        /// </summary>
        OPTIMIZATION_NA = 0,
        /// <summary>
        /// 自动优化
        /// </summary>
        OPTIMIZATION_AUTO,
        /// <summary>
        /// 手动优化
        /// </summary>
        OPTIMIZATION_MANUAL,
        Last
    }

    /// <summary>分析方式
    /// 
    /// </summary>
    [Serializable]
    public enum EAnalysisType
    {
        /// <summary>
        /// 定性
        /// </summary>
        ANALYSIS_QUALITATIVE = 1,
        /// <summary>
        /// 定量
        /// </summary>
        ANALYSIS_QUANTITATIVE,
        /// <summary>
        /// 最后标志
        /// </summary>
        Last
    }

    /// <summary>
    /// HRM参数:分型方法
    /// </summary>
    [Serializable]
    public enum EHRMTypeMethod
    {
        Auto = 0,
        Standart
    }

    /// <summary>
    /// HRM参数:分类方法
    /// </summary>
    [Serializable]
    public enum EHRMClassificationMethod
    {
        System = 0,
        Fixed
    }

    public enum EADTypeMethod
    {
        Auto = 0,
        Standart,
        Custom
    }

    public enum EPeekType
    {
        Positive,
        Negative,
        Positive_Negative
    }

    public enum EDigitalFilter
    {
        /// <summary>
        /// 不滤波
        /// </summary>
        None = 0,
        /// <summary>
        /// 轻度滤波
        /// </summary>
        Normal,
        /// <summary>
        /// 重度滤波
        /// </summary>
        High,
    }

    #region 规则

    [Serializable]
    public enum EExpressionType
    {
        /// <summary>
        /// 检测数据
        /// </summary>
        Detection = 0,
        /// <summary>
        /// 中间变量
        /// </summary>
        MidExpression,
        /// <summary>
        /// 操作符
        /// </summary>
        Operator,
        /// <summary>
        /// 常量
        /// </summary>
        String
    }

    [Serializable]
    public enum EMidExpressionType
    {
        /// <summary>
        /// 检测数据
        /// </summary>
        Detection = 0,
        /// <summary>
        /// 操作符
        /// </summary>
        Operator,
        /// <summary>
        /// 常量
        /// </summary>
        String
    }
    #endregion

    #region 规则-New

    [Serializable]
    public enum ERuleType
    {
        Experiment_QC,
        Sample_QC,
        Resule_Judge,
    }

    [Serializable]
    public enum EBlockType
    {
        Null,
        JudgeBlock,
        ResultBlock,
        ReferenceBlock,
        MidValueBlock,
    }

    [Serializable]
    public enum EProperty
    {
        SampleType = 0,
        Target,
        Tube,
        Channel,
        Property,
    }

    [Serializable]
    public enum EMathOperator
    {
        Plus = 0,
        Minus,
        Multiple,
        Divide,
    }

    [Serializable]
    public enum EOperator
    {
        Null = 0,
        Equal,
        UnEqual,
        LessThan,
        EqualLessThan,
        MoreThan,
        EqualMoreThan,
    }
    [Serializable]
    public enum ERuleDecorate
    {
        AllTarget = 0,
        AnyTarget,
        EveryTarget,
        CustomTarget,
        AnyTubeAllTarget,
        AllTubeAnyTarget,
        EveryTubeAnyTarget,
        EveryTubeAllTarget,
        AllTube,
        AnyTube,
        EveryTube,
        CustomTube,
        Custom,
    }
    
    [Serializable]
    public enum ECalcResult
    {
        None,
        Ct,
        Cn,
        Rn,
        r,
    }

    [Serializable]
    public enum EMidValue
    {
        Sample,
        Well,
        Target,
    }

    #endregion

}
