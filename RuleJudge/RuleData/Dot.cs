﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace SLANCommon
{
    [Serializable]
    public class Dot
    {
        private double _X;

        public double X
        {
            get { return _X; }
            set { if (_X == value) return; _X = value; }
        }

        private double _Y;

        public double Y
        {
            get { return _Y; }
            set { if (_Y == value) return; _Y = value; }
        }

        public Dot()
        {
        }

        public Dot(Dot dot)
        {
            X = dot.X;
            Y = dot.Y;
        }

        public override string ToString()
        {
            return string.Format("X={0}, Y={1}", X, Y);
        }
    }
}
