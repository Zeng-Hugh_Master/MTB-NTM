﻿using System.Collections.Generic;
using System.Windows.Media;
using System.Linq;
using RuleJudge.Judge;
using SLANCommon.Enum;
using SLANCommon;

namespace RuleJudge.RuleData
{
    /// <summary>
    /// 样本类
    /// </summary>
    public class Sample
    {
        /// <summary>
        /// 标示
        /// </summary>
        public int Key { get; set; }
        /// <summary>
        /// 样本唯一标识,同患者信息中ID
        /// </summary>
        public string ID { get; set; }
        /// <summary>
        /// 样本类型
        /// </summary>
        public SampleType Type { get; set; }
        /// <summary>
        /// 样本属性
        /// </summary>
        public string Property { get; set; }
        /// <summary>
        /// 样本复管号
        /// </summary>
        public string MultiTubeNo { get; set; }
        /// <summary>
        /// 样本所包含的所有管
        /// </summary>
        public List<Tube> Tubes { get; set; }
        /// <summary>
        /// 样本所包含的所有目标
        /// </summary>
        public List<Target> Targets
        {
            get
            {
                List<Target> list = new List<Target>();
                foreach (Tube tube in Tubes)
                {
                    list.AddRange(tube.Targets);
                }
                return list;
            }
        }
        /// <summary>
        /// 样本结论
        /// </summary>
        public List<Conclusion> Conclusion { get; set; }
        /// <summary>
        /// 用户自定义的样本结果
        /// </summary>
        public List<CustomResult> CustomResults { get; set; }
        private PatientInfo _PatientInfo = new PatientInfo();
        /// <summary>
        /// 患者信息
        /// </summary>
        public PatientInfo PatientInfo
        {
            get { return _PatientInfo; }
            set { _PatientInfo = value; }
        }

        public Sample()
        {
            Tubes = new List<Tube>();
            Conclusion = new List<Conclusion>();
            CustomResults = new List<CustomResult>();
        }
    }
    /// <summary>
    /// 患者信息
    /// </summary>
    public class PatientInfo
    {
        /// <summary>
        /// 唯一标示
        /// </summary>
        public string ID { get; set; }
        /// <summary>
        /// 姓名
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 性别
        /// </summary>
        public string Sex { get; set; }
        /// <summary>
        /// 年龄
        /// </summary>
        public string Age { get; set; }
        /// <summary>
        /// 标本类型
        /// </summary>
        public string Specimen { get; set; }
        /// <summary>
        /// 采样日期
        /// </summary>
        public string SpecimenDate { get; set; }
        /// <summary>
        /// 送检医生
        /// </summary>
        public string Doctor { get; set; }
        /// <summary>
        /// 送检科室
        /// </summary>
        public string Office { get; set; }
        /// <summary>
        /// 诊断
        /// </summary>
        public string Diagnosis { get; set; }
        /// <summary>
        /// 病历号
        /// </summary>
        public string CaseID { get; set; }
        /// <summary>
        /// 病床号
        /// </summary>
        public string BedID { get; set; }
        /// <summary>
        /// 住院号
        /// </summary>
        public string InPatientID { get; set; }
        /// <summary>
        /// 门诊号
        /// </summary>
        public string OutPatientID { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }
    }

    /// <summary>
    /// 管类
    /// </summary>
    public class Tube
    {
        /// <summary>
        /// 标示
        /// </summary>
        public int Key { get; set; }
        /// <summary>
        /// 管号
        /// </summary>
        public int ID { get; set; }
        /// <summary>
        /// 管名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 管所包含的目标
        /// </summary>
        public List<Target> Targets { get; set; }
        /// <summary>
        /// 管结论
        /// </summary>
        public List<Conclusion> Conclusion { get; set; }
        /// <summary>
        /// 用户自定义的管结果
        /// </summary>
        public List<CustomResult> CustomResults { get; set; }
        /// <summary>
        /// 直角坐标曲线
        /// </summary>
        public List<Dot> RightAngleCurve { get; set; }
        /// <summary>
        /// 极坐标曲线
        /// </summary>
        public List<Dot> PolarCurve { get; set; }

        public Tube()
        {
            Targets = new List<Target>();
            Conclusion = new List<Conclusion>();
            CustomResults = new List<CustomResult>();
        }
    }

    /// <summary>
    /// （检测）目标类
    /// </summary>
    public class Target
    {
        /// <summary>
        /// 标示
        /// </summary>
        public int Key { get; set; }
        /// <summary>
        /// 目标名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 目标所在管的管名
        /// </summary>
        public string TubeName { get; set; }
        /// <summary>
        /// 目标对应的通道号
        /// </summary>
        public int ChannelNo { get; set; }
        /// <summary>
        /// 目标对应的染料名
        /// </summary>
        public string Dye { get; set; }
        /// <summary>
        /// 目标结论
        /// </summary>
        public List<Conclusion> Conclusion { get; set; }
        /// <summary>
        /// 用户自定义的目标结果
        /// </summary>
        public List<CustomResult> CustomResults { get; set; }
        /// <summary>
        /// 目标包含的熔解峰
        /// </summary>
        public List<MeltingPeak> Peaks { get; set; }
        /// <summary>
        /// 目标检测结果
        /// </summary>
        public TestResult Result { get; set; }
        public Target()
        {
            Peaks = new List<MeltingPeak>();
            Conclusion = new List<Conclusion>();
            CustomResults = new List<CustomResult>();
            Result = new TestResult();
        }
    }
    /// <summary>
    /// 目标检测结果类
    /// </summary>
    public partial class TestResult
    {
        /// <summary>
        /// Ct值
        /// </summary>
        public double Ct { get; set; }
        public double CtMean { get; set; }
        public double CtSD { get; set; }
        public double CtCV { get; set; }
        /// <summary>
        /// 浓度值
        /// </summary>
        public double Cn { get; set; }
        public double CnMean { get; set; }
        public double CnSD { get; set; }
        public double CnCV { get; set; }
        /// <summary>
        /// 荧光终点值
        /// </summary>
        public double Rn { get; set; }
        /// <summary>
        /// 相关系数
        /// </summary>
        public double r { get; set; }
        /// <summary>
        /// 原始曲线
        /// </summary>
        public List<Dot> RawCurve { get; set; }
        /// <summary>
        /// 扩增曲线
        /// </summary>
        public List<Dot> AMPCurve { get; set; }
        /// <summary>
        /// 原始溶解曲线
        /// </summary>
        public List<Dot> RawMeltingCurve { get; set; }
        /// <summary>
        /// 峰值曲线
        /// </summary>
        public List<Dot> PeakCurve { get; set; }
        /// <summary>
        /// 规格化峰值曲线
        /// </summary>
        public List<Dot> NormalizationPeakCurve { get; set; }
        /// <summary>
        /// 溶解曲线导数曲线
        /// </summary>
        public List<Dot> DerivativeCurve { get; set; }
        /// <summary>
        /// 规格化溶解曲线
        /// </summary>
        public List<Dot> NormalizationCurve { get; set; }
        /// <summary>
        /// 温度Shift曲线
        /// </summary>
        public List<Dot> ShiftCurve { get; set; }
        /// <summary>
        /// 溶解差分曲线
        /// </summary>
        public List<Dot> DifferenceCurve { get; set; }
    }

    /// <summary>
    /// 结论类
    /// </summary>
    public class Conclusion
    {
        /// <summary>
        /// 结论内容
        /// </summary>
        public string Content{get;set;}
        /// <summary>
        /// 显示颜色
        /// </summary>
        public Color Color { get; set; }
        // <summary>
        /// 背景颜色
        /// </summary>
        public Color ForegroundColor { get; set; }
        /// <summary>
        /// 背景颜色
        /// </summary>
        public Color BackgroundColor { get; set; }

        public Conclusion()
        {
            Color = Colors.Transparent;
            BackgroundColor = Colors.Transparent;
            ForegroundColor = Colors.Transparent;
        }
    }
    /// <summary>
    /// 熔解峰类
    /// </summary>
    public class MeltingPeak
    {
        /// <summary>
        /// 标示
        /// </summary>
        public int Key { get; set; }
        /// <summary>
        /// 熔解温度值
        /// </summary>
        public double Tm { get; set; }
        /// <summary>
        /// 熔解峰高度
        /// </summary>
        public double Rm { get; set; }
        /// <summary>
        /// 是否为未知熔解峰：在规则判断中赋值
        /// </summary>
        public bool IsUnKnown { get; set; }
        /// <summary>
        /// 峰宽
        /// </summary>
        public double Width { get; set; }
        /// <summary>
        /// YValue
        /// </summary>
        public double YValue { get; set; }

        public MeltingPeak()
        {
            Key = -1;//新增熔解峰，设为-1。
        }
    }

    /// <summary>
    /// 用户自定义结果
    /// </summary>
    public class CustomResult
    {
        /// <summary>
        /// 结果名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 文字内容
        /// </summary>
        public string Content { get; set; }
        /// <summary>
        /// 结果值，废弃
        /// </summary>
        public double Value { get; set; }
        /// <summary>
        /// 小数位数，废弃
        /// </summary>
        public int DecimalDigits { get; set; }
    }
    /// <summary>
    /// 熔解峰定义
    /// </summary>
    public class PeakDefine
    {
        /// <summary>
        /// 所处目标的名称
        /// </summary>
        public string TargetName { get; set; }
        /// <summary>
        /// 峰值（Tm值）
        /// </summary>
        public double PeakValue { get; set; }
        /// <summary>
        /// 峰值范围
        /// </summary>
        public double PeakValueRange { get; set; }
        /// <summary>
        /// 峰的名称
        /// </summary>
        public string PeakName { get; set; }
        public PeakDefine(string _TargetName, double _PeakValue, double _PeakValueRange, string _PeakName)
        {
            TargetName = _TargetName;
            PeakValue = _PeakValue;
            PeakValueRange = _PeakValueRange;
            PeakName = _PeakName;
        }
    }
    /// <summary>
    /// 定性定量参数
    /// </summary>
    public class Paramter
    {
        /// <summary>
        /// 目标名称
        /// </summary>
        public string TargetName { get; set; }
        /// <summary>
        /// 分析类型
        /// </summary>
        public EAnalysisType AnalysisType { get; set; }
        /// <summary>
        /// 基线起点
        /// </summary>
        public int BeginBaseline { get; set; }
        /// <summary>
        /// 基线终点
        /// </summary>
        public int EndBaseline { get; set; }
        /// <summary>
        /// 自动阈值
        /// </summary>
        public bool AutoThreshold { get; set; }
        /// <summary>
        /// 手动阈值
        /// </summary>
        public double Threshold { get; set; }
        /// <summary>
        /// 基线优化
        /// </summary>
        public EOptionzationMode OptimizationMode { get; set; }
        /// <summary>
        /// 数字滤波
        /// </summary>
        public bool DigitalFilter { get; set; }
        /// <summary>
        /// 检测下限
        /// </summary>
        public double DetectionMin { get; set; }
        /// <summary>
        /// 检测上限
        /// </summary>
        public double DetectionMax { get; set; }
        /// <summary>
        /// 扩增曲线增益
        /// </summary>
        public double AMPGain { get; set; }
        /// <summary>
        /// 定量参数A
        /// </summary>
        public double ParamterA { get; set; }
        /// <summary>
        /// 定量参数B
        /// </summary>
        public double ParamterB { get; set; }
    }
    /// <summary>
    /// 溶解曲线参数
    /// </summary>
    public class MeltParamter : Paramter
    {
        /// <summary>
        /// 最小温度
        /// </summary>
        public double MinTemperature { get; set; }
        /// <summary>
        /// 最大温度
        /// </summary>
        public double MaxTemperature { get; set; }
        /// <summary>
        /// 噪声阈值
        /// </summary>
        public double NoiseThreshold { get; set; }
        /// <summary>
        /// 峰宽阈值
        /// </summary>
        public double PeekWidthThreshold { get; set; }
        /// <summary>
        /// 峰高阈值
        /// </summary>
        public double PeekHightThreshold { get; set; }
    }
    /// <summary>
    /// HRM参数
    /// </summary>
    public class HRMParamter : MeltParamter
    {
        /// <summary>
        /// 熔解前基线起始温度
        /// </summary>
        public double StartTemperatureBefore { get; set; }
        /// <summary>
        /// 熔解前基线结束温度
        /// </summary>
        public double EndTemperatureBefore { get; set; }
        /// <summary>
        /// 熔解后基线起始温度
        /// </summary>
        public double StartTemperatureAfter { get; set; }
        /// <summary>
        /// 熔解后基线结束温度
        /// </summary>
        public double EndTemperatureAfter { get; set; }
        /// <summary>
        /// 品质阈值
        /// </summary>
        public double QualityThreshold { get; set; }
        /// <summary>
        /// Shift阈值
        /// </summary>
        public double TemperatureShiftThreshold { get; set; }
        /// <summary>
        /// 分型方法
        /// </summary>
        public EHRMTypeMethod TypeMethod { get; set; }
        /// <summary>
        /// 废弃
        /// </summary>
        public EHRMClassificationMethod ClassificationMethod { get; set; }
        /// <summary>
        /// 分类阈值
        /// </summary>
        public double ClassThreshold { get; set; }
    }
    /// <summary>
    /// 等温基因参数
    /// </summary>
    public class ADParamter : Paramter
    {
        /// <summary>
        /// 品质阈值
        /// </summary>
        public double QualityThreshold { get; set; }
        /// <summary>
        /// 增益
        /// </summary>
        public double GainRatio { get; set; }
        /// <summary>
        /// 分型方法
        /// </summary>
        public EADTypeMethod TypeMethod { get; set; }
    }
}
