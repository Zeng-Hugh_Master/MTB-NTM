﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SLANCommon.Enum
{
    /// <summary>
    /// 样本类型
    /// </summary>
    [Serializable]
    public enum SampleType
    {
        Current = -100,
        All,
        First = -1,
        /// <summary>
        /// 样品
        /// </summary>
        Unknown,
        /// <summary>
        /// 标准品
        /// </summary>
        Standard,
        /// <summary>
        /// 阳性对照
        /// </summary>
        HighPositive,
        /// <summary>
        /// 临界阳性对照
        /// </summary>
        LowPositive,
        /// <summary>
        /// 阴性对照
        /// </summary>
        Negative,
        /// <summary>
        /// 无模板对照
        /// </summary>
        NTC,
        /// <summary>
        /// 校准品（质控品）
        /// </summary>
        QC,
        /// <summary>
        /// 重测样品
        /// </summary>
        RetestSample,

        /// <summary>
        /// 最后（只用作标识）
        /// </summary>
        Last
    }

    /// <summary>
    /// 温度控制方式
    /// </summary>
    public enum ETemperatureControlMode
    {
        /// <summary>
        /// 模块控温
        /// </summary>
        Module = 0,
        /// <summary>
        /// 试管控温
        /// </summary>
        Tube,
        /// <summary>
        /// Fast控温
        /// </summary>
        Fast
    }

    /// <summary>
    /// 通道扫描方式
    /// </summary>
    public enum EScanMode
    {
        None = -1,
        /// <summary>
        /// 按项目
        /// </summary>
        Project = 0,
        /// <summary>
        /// 全通道
        /// </summary>
        All
    }

    /// <summary>
    /// 实验状态
    /// </summary>
    public enum EExperimentStatus
    {
        /// <summary>
        /// 未运行
        /// </summary>
        NotRunning = 0,
        /// <summary>
        /// 正在运行
        /// </summary>
        Running,
        /// <summary>
        /// 中断
        /// </summary>
        Interrupt,
        /// <summary>
        /// 异常中断
        /// </summary>
        AbnormalInterrupt,
        /// <summary>
        /// 结束
        /// </summary>
        Completed,
    }

    /// <summary>
    /// 扩增曲线算法选择
    /// </summary>
    [Serializable]
    public enum EAMPAlgorithm
    {
        Default = 0,
        Subtraction,
        RelativeLinear,
    }

    [Serializable]
    public enum EChannelSelectMode
    {
        SingleClick = 0,
        MultiClickCtl,
    }

    [Serializable]
    public enum EColorMode
    {
        Channel = 0,
        Type,
        Row,
        Column,
        Result,
        Target,
    }

    [Serializable]
    public enum ECurveMaxYMode
    {
        AllWells = 0,
        SelectedChannel,
        SelectedProject,
        SelectedWells,
        SelectedSeries,
    }

    [Serializable]
    public enum EWellSelectMode
    {
        Sample = 0,
        Well,
    }

    /// <summary>
    /// 样本信息表行选择方式
    /// </summary>
    public enum EDataRowSelectMode
    {
        Target = 0,
        Well,
        Sample,
        Cell,
    }
    /// <summary>
    /// 行间距模式
    /// </summary>
    public enum ERowSpaceMode
    {
        Null = 0,
        Well,
        Sample,
    }

    [Serializable]
    public enum EDetectionType
    {
        Rn = 0,
        Cn,
    }

    /// <summary>
    /// 排序方式
    /// </summary>
    [Serializable]
    public enum ESampleSortMode
    {
        Column = 0,
        Row,
        Module,
        Custom,
    }

    public enum ERefMode
    {
        Auto = 0,
        Manual,
    }

    public enum ESizeMode
    {
        Small = 0,
        Middle,
        Large,
    }

    public enum EWellDisplayMode
    {
        Null = -100,
        ProjectName = 0,
        SampleID,
        PatientName,
        TubeName,
        SampleName,
        PatientInfoForWell,
    }

    /// <summary>
    /// HRM基因分型编号
    /// </summary>
    public enum EGenoType
    {
        GenoBadWell = -2,
        GenoNegative = -1,
        GenoUnknown = 0,
        GenoType1,
        GenoType2,
        GenoType3,
        GenoType4,
        GenoType5,
        GenoType6,
    }

    /// <summary>
    /// AD基因分型编号
    /// </summary>
    public enum EADGenoType
    {
        GenoBadWell = -2,
        GenoNegative = -1,
        GenoUnknown = 0,
        GenoType1,
        GenoType2,
        GenoType3,
    }

    /// <summary>
    /// 曲线显示模式
    /// </summary>
    public enum ESeriesType
    {
        Null = -1,
        None = 0,
        Circle = 6,
        Square = 5,
        Triangle = 4,
        X = 3,

        Dotted = 20,
        Dashed,
        DashDot,
    }

    /// <summary>
    /// 患者信息保存模式
    /// </summary>
    public enum EPatientInfoSaveType
    {
        None = -1,
        All,
        PatientInfo_Index,
    }
}
