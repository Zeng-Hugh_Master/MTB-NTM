﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

    public class BaseStream : MemoryStream
    {
        public BaseStream()
        {

        }

        public virtual string ReadString()
        {
            int len = ReadInt();
            byte[] b = new byte[len];
            base.Read(b, 0, len);
            return BytesConverter.BytesToString(b);
        }
        public virtual double ReadDouble()
        {
            byte[] b = new byte[8];
            base.Read(b, 0, 8);
            return BytesConverter.BytesToDouble(b);
        }
        public virtual int ReadInt()
        {
            byte[] b = new byte[4];
            base.Read(b, 0, 4);
            return BytesConverter.BytesToInt(b);
        }
        public new virtual byte ReadByte()
        {
            return (byte)base.ReadByte();
        }
        public virtual byte[] ReadBytes(int len)
        {
            byte[] buffer = new byte[len];
            base.Read(buffer, 0, buffer.Length);
            return buffer;
        }
        public virtual bool ReadBool()
        {
            return BytesConverter.BytesToBool(ReadByte());
        }
        public virtual long ReadLong()
        {
            byte[] b = new byte[8];
            base.Read(b, 0, 8);
            return BytesConverter.BytesToLong(b);
        }

        public virtual void Write(long value)
        {
            Write(BytesConverter.GetBytes(value));
        }
        public virtual void Write(byte[] array)
        {
            base.Write(array, 0, array.Length);
        }
        public virtual void Write(bool value)
        {
            Write(BytesConverter.GetBytes(value));
        }
        public virtual void Write(int value)
        {
            Write(BytesConverter.GetBytes(value));
        }
        public virtual void Write(double value)
        {
            Write(BytesConverter.GetBytes(value));
        }
        public virtual void Write(string value)
        {
            byte[] b = BytesConverter.GetBytes(value);
            Write(b.Length);
            Write(b);
        }
    }
