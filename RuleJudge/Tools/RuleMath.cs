﻿using System.Collections.Generic;
using RuleJudge.RuleData;

namespace RuleJudge.Tools
{
    public class RuleMath
    {
        /// <summary>
        /// 根据Tm值判其是否属于某个峰（middleValue为峰中心值，+/-range为峰范围）
        /// </summary>
        /// <param name="Tm"></param>
        /// <param name="middleValue"></param>
        /// <param name="range"></param>
        /// <returns></returns>
        public static bool PeakFit(double Tm, double middleValue, double range)
        {
            if (middleValue - range <= Tm && Tm < middleValue + range)
                return true;
            else
                return false;
        }

        /// <summary>
        /// 判熔解峰列表中是否包含某个峰（middleValue为峰中心值，+/-range为峰范围）
        /// </summary>
        /// <param name="peaks"></param>
        /// <param name="middleValue"></param>
        /// <param name="range"></param>
        /// <returns></returns>
        public static bool PeakContain(List<MeltingPeak> peaks, double middleValue, double range)
        {
            foreach (MeltingPeak p in peaks)
            {
                if (PeakFit(p.Tm, middleValue, range))
                    return true;
            }
            return false;
        }
    }
}