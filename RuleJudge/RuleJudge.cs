﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;
using RuleJudge.RuleData;
using System.IO;
using RuleJudge.Tools;
using System.Windows;
using RuleJudge.Judge;
using SLANCommon.Enum;

namespace RuleJudge
{
    public class RuleJudge : BaseRuleJudge
    {
        //设置规则类
        BaseJudge rule = new MTB_NTM();

        //以下为本判断规则的一些基本信息，用于与引用它的项目进行符合性判断，来确定其是否能被正确引用。
        const string AllowMinVersion = "8.2.2.50204";
        double MinVersion = double.Parse(AllowMinVersion.Replace(".", ""));

        /// <summary>
        /// 结论判断
        /// </summary>
        /// <returns>
        /// Null ：判断正确
        /// Other：判断错误，返回对应错误信息
        /// </returns>
        protected override string Judge()
        {
            rule.Paramters = _paramters;
            return rule.Judge(_samples);
        }

        //以下为：与判断过程相关的一些方法

        /// <summary>
        /// 项目验证：包括版本验证、实验类型验证、检测目标验证；用于确认本判断规则是否与引用它的项目匹配。外部界面调用。
        /// </summary>
        /// <param name="version">主程序版本，double</param>
        /// <param name="projectType">项目类型，int</param>
        /// <param name="targets">项目目标集合</param>
        /// <param name="channels">通道号</param>
        /// <param name="tubes">管号</param>
        /// <returns>
        /// Null ：匹配
        /// Other：不匹配，返回对应错误信息
        /// </returns>
        public override string Validate(double version, EProjectType projectType, string[] targets, int[] channels, int[] tubes)
        {
            string info = VersionValidate(version);
            if (info != null) return info;                  //若软件版本不匹配，则返回软件版本不匹配信息
            if (rule.Language == 0)
                info = "您所选的判断规则与当前项目不匹配！";
            else
                info = "The rule file you selected does not match the current project!";
            if (projectType != rule.Type) return info;           //实验类型不匹配，则返回项目不匹配信息
            int count = targets.Count();                    //目标数量不匹配，则返回项目不匹配信息
            if (rule.Targets != null && rule.Targets.Count > 0 && rule.Targets.Count != count) return info;
            if (rule.ChannelsNo.Count != count || rule.TubesNo.Count != count) return info;
            for (int i = 0; i < count; i++)                 //若有一个检测目标的其中一个信息（目标名称、通道编号、试管编号）不匹配，则返回项目不匹配信息
            {
                if (rule.Targets != null && rule.Targets.Count > 0 && rule.Targets[i] != targets[i]) return info;
                if (channels[i] != rule.ChannelsNo[i]) return info;
                if (tubes[i] != rule.TubesNo[i]) return info;
            }
            return null;
        }

        /// <summary>
        /// 版本验证，验证主程序版本和规则判断版本是否匹配
        /// </summary>
        /// <param name="version"></param>
        /// <returns></returns>
        protected override string VersionValidate(double version)
        {
            if (version < MinVersion)                  //若软件版本不匹配，则返回软件版本不匹配信息
            {
                if (rule.Language == 0)
                    return "当前软件版本过旧，请更新为Ver " + AllowMinVersion + "或以上！";
                else
                    return "Old software version detected, please update your software to Ver " + AllowMinVersion + " or higher!";
            }
            else
            {
                return null;
            }
        }

        protected override EProjectType GetType()
        {
            return rule.Type;
        }
    }
}
